package common

const (
	SignV4Algorithm = "AWS4-HMAC-SHA256"
	Iso8601Format   = "20060102T150405Z"
	AwsDateFormat   = "20060102"
)
