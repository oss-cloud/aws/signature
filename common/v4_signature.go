package common

import (
	"context"
	"crypto/hmac"
	"crypto/sha256"
	"encoding/hex"
	"time"

	"go.opentelemetry.io/otel/api/global"
)

var v4Tracer = global.Tracer("v4-signature")

// getSigningKey hmac seed to calculate final signature.
func GetSigningKey(ctx context.Context, secretKey string, t time.Time, region, serviceName string) []byte {
	ctx, span := v4Tracer.Start(ctx, "GetSigningKey")
	defer span.End()

	date := sumHMAC(ctx, []byte("AWS4"+secretKey), []byte(t.Format(AwsDateFormat)))
	regionBytes := sumHMAC(ctx, date, []byte(region))
	service := sumHMAC(ctx, regionBytes, []byte(serviceName))
	signingKey := sumHMAC(ctx, service, []byte("aws4_request"))
	return signingKey
}

// getSignature final signature in hexadecimal form.
func GetSignature(ctx context.Context, signingKey []byte, stringToSign string) string {
	ctx, span := v4Tracer.Start(ctx, "GetSigningKey")
	defer span.End()

	return hex.EncodeToString(sumHMAC(ctx, signingKey, []byte(stringToSign)))
}

// sumHMAC calculate hmac between two input byte array.
func sumHMAC(ctx context.Context, key []byte, data []byte) []byte {
	ctx, span := v4Tracer.Start(ctx, "sumHMAC")
	defer span.End()

	hash := hmac.New(sha256.New, key)
	hash.Write(data)
	return hash.Sum(nil)
}

// getStringToSign a string based on selected query values.
func GetStringToSign(ctx context.Context, canonicalRequest string, t time.Time, scope string) string {
	ctx, span := v4Tracer.Start(ctx, "GetStringToSign")
	defer span.End()

	stringToSign := SignV4Algorithm + "\n" + t.Format(Iso8601Format) + "\n"
	stringToSign = stringToSign + scope + "\n"
	canonicalRequestBytes := sha256.Sum256([]byte(canonicalRequest))
	stringToSign = stringToSign + hex.EncodeToString(canonicalRequestBytes[:])
	return stringToSign
}
