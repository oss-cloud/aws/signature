package validate

import (
	"context"
	"net/http"
	"time"
)

type Validator interface {
	Validate(ctx context.Context, r *http.Request) (accessKeyID string, err error)
}

type KeyLocator interface {
	FetchSecretKey(ctx context.Context, accessKeyID, sessionToken string) (secretKey string, err error)
}

type ClockFunc func() time.Time
