package validate

import (
	"bytes"
	"context"
	"fmt"
	"io"
	"net/http"
	"net/url"
	"strings"
	"testing"
	"time"
)

var now = time.Date(2015, time.August, 30, 12, 36, 0, 0, time.UTC)
var clockFunc ClockFunc = func() time.Time { return now }

type fixedLocator struct{}

func (l *fixedLocator) FetchSecretKey(_ context.Context, _, _ string) (secretKey string, err error) {
	return "wJalrXUtnFEMI/K7MDENG+bPxRfiCYEXAMPLEKEY", nil
}

func TestValidator(t *testing.T) {
	validator := &V4Validator{
		Service:  "service",
		Location: "us-east-1",
		Locator:  new(fixedLocator),
		Clock:    clockFunc,
	}

	for idx, tc := range []http.Request{
		// get-header-key-duplicate
		{
			URL:    &url.URL{Path: "/"},
			Method: http.MethodGet,
			Header: map[string][]string{
				"Host":          {"example.amazonaws.com"},
				"My-Header1":    {"value2", "value2", "value1"},
				"X-Amz-Date":    {"20150830T123600Z"},
				"Authorization": {"AWS4-HMAC-SHA256 Credential=AKIDEXAMPLE/20150830/us-east-1/service/aws4_request, SignedHeaders=host;my-header1;x-amz-date, Signature=c9d5ea9f3f72853aea855b47ea873832890dbdd183b4468f858259531a5138ea"},
			},
			Body: io.NopCloser(bytes.NewReader(nil)),
		},
		// get-header-value-order
		{
			URL:    &url.URL{Path: "/"},
			Method: http.MethodGet,
			Header: map[string][]string{
				"Host":          {"example.amazonaws.com"},
				"My-Header1":    {"value4", "value1", "value3", "value2"},
				"X-Amz-Date":    {"20150830T123600Z"},
				"Authorization": {"AWS4-HMAC-SHA256 Credential=AKIDEXAMPLE/20150830/us-east-1/service/aws4_request, SignedHeaders=host;my-header1;x-amz-date, Signature=08c7e5a9acfcfeb3ab6b2185e75ce8b1deb5e634ec47601a50643f830c755c01"},
			},
			Body: io.NopCloser(bytes.NewReader(nil)),
		},
		// get-header-value-trim
		{
			URL:    &url.URL{Path: "/"},
			Method: http.MethodGet,
			Header: map[string][]string{
				"Host":          {"example.amazonaws.com"},
				"My-Header1":    {"value1"},
				"My-Header2":    {`"a   b   c"`},
				"X-Amz-Date":    {"20150830T123600Z"},
				"Authorization": {"AWS4-HMAC-SHA256 Credential=AKIDEXAMPLE/20150830/us-east-1/service/aws4_request, SignedHeaders=host;my-header1;my-header2;x-amz-date, Signature=acc3ed3afb60bb290fc8d2dd0098b9911fcaa05412b367055dee359757a9c736"},
			},
			Body: io.NopCloser(bytes.NewReader(nil)),
		},
		// get-unreserved
		{
			URL:    &url.URL{Path: "/-._~0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"},
			Method: http.MethodGet,
			Header: map[string][]string{
				"Host":          {"example.amazonaws.com"},
				"X-Amz-Date":    {"20150830T123600Z"},
				"Authorization": {"AWS4-HMAC-SHA256 Credential=AKIDEXAMPLE/20150830/us-east-1/service/aws4_request, SignedHeaders=host;x-amz-date, Signature=07ef7494c76fa4850883e2b006601f940f8a34d404d0cfa977f52a65bbf5f24f"},
			},
			Body: io.NopCloser(bytes.NewReader(nil)),
		},
		// get-utf8
		{
			URL:    &url.URL{Path: "/ሴ"},
			Method: http.MethodGet,
			Header: map[string][]string{
				"Host":          {"example.amazonaws.com"},
				"X-Amz-Date":    {"20150830T123600Z"},
				"Authorization": {"AWS4-HMAC-SHA256 Credential=AKIDEXAMPLE/20150830/us-east-1/service/aws4_request, SignedHeaders=host;x-amz-date, Signature=8318018e0b0f223aa2bbf98705b62bb787dc9c0e678f255a891fd03141be5d85"},
			},
			Body: io.NopCloser(bytes.NewReader(nil)),
		},
		// get-vanilla
		{
			URL:    &url.URL{Path: "/"},
			Method: http.MethodGet,
			Header: map[string][]string{
				"Host":          {"example.amazonaws.com"},
				"X-Amz-Date":    {"20150830T123600Z"},
				"Authorization": {"AWS4-HMAC-SHA256 Credential=AKIDEXAMPLE/20150830/us-east-1/service/aws4_request, SignedHeaders=host;x-amz-date, Signature=5fa00fa31553b73ebf1942676e86291e8372ff2a2260956d9b8aae1d763fbf31"},
			},
			Body: io.NopCloser(bytes.NewReader(nil)),
		},
		// get-vanilla-empty-query-key
		{
			URL:    &url.URL{Path: "/", RawQuery: "Param1=value1"},
			Method: http.MethodGet,
			Header: map[string][]string{
				"Host":          {"example.amazonaws.com"},
				"X-Amz-Date":    {"20150830T123600Z"},
				"Authorization": {"AWS4-HMAC-SHA256 Credential=AKIDEXAMPLE/20150830/us-east-1/service/aws4_request, SignedHeaders=host;x-amz-date, Signature=a67d582fa61cc504c4bae71f336f98b97f1ea3c7a6bfe1b6e45aec72011b9aeb"},
			},
			Body: io.NopCloser(bytes.NewReader(nil)),
		},
		// get-vanilla-query
		{
			URL:    &url.URL{Path: "/"},
			Method: http.MethodGet,
			Header: map[string][]string{
				"Host":          {"example.amazonaws.com"},
				"X-Amz-Date":    {"20150830T123600Z"},
				"Authorization": {"AWS4-HMAC-SHA256 Credential=AKIDEXAMPLE/20150830/us-east-1/service/aws4_request, SignedHeaders=host;x-amz-date, Signature=5fa00fa31553b73ebf1942676e86291e8372ff2a2260956d9b8aae1d763fbf31"},
			},
			Body: io.NopCloser(bytes.NewReader(nil)),
		},
		// get-vanilla-query-order-key
		{
			URL:    &url.URL{Path: "/", RawQuery: "Param1=value2&Param1=Value1"},
			Method: http.MethodGet,
			Header: map[string][]string{
				"Host":          {"example.amazonaws.com"},
				"X-Amz-Date":    {"20150830T123600Z"},
				"Authorization": {"AWS4-HMAC-SHA256 Credential=AKIDEXAMPLE/20150830/us-east-1/service/aws4_request, SignedHeaders=host;x-amz-date, Signature=eedbc4e291e521cf13422ffca22be7d2eb8146eecf653089df300a15b2382bd1"},
			},
			Body: io.NopCloser(bytes.NewReader(nil)),
		},
		// get-vanilla-query-order-key-case
		{
			URL:    &url.URL{Path: "/", RawQuery: "Param2=value2&Param1=value1"},
			Method: http.MethodGet,
			Header: map[string][]string{
				"Host":          {"example.amazonaws.com"},
				"X-Amz-Date":    {"20150830T123600Z"},
				"Authorization": {"AWS4-HMAC-SHA256 Credential=AKIDEXAMPLE/20150830/us-east-1/service/aws4_request, SignedHeaders=host;x-amz-date, Signature=b97d918cfa904a5beff61c982a1b6f458b799221646efd99d3219ec94cdf2500"},
			},
			Body: io.NopCloser(bytes.NewReader(nil)),
		},
		// get-vanilla-query-order-value
		{
			URL:    &url.URL{Path: "/", RawQuery: "Param1=value2&Param1=value1"},
			Method: http.MethodGet,
			Header: map[string][]string{
				"Host":          {"example.amazonaws.com"},
				"X-Amz-Date":    {"20150830T123600Z"},
				"Authorization": {"AWS4-HMAC-SHA256 Credential=AKIDEXAMPLE/20150830/us-east-1/service/aws4_request, SignedHeaders=host;x-amz-date, Signature=5772eed61e12b33fae39ee5e7012498b51d56abc0abb7c60486157bd471c4694"},
			},
			Body: io.NopCloser(bytes.NewReader(nil)),
		},
		// get-vanilla-query-unreserved
		{
			URL:    &url.URL{Path: "/", RawQuery: "-._~0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz=-._~0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"},
			Method: http.MethodGet,
			Header: map[string][]string{
				"Host":          {"example.amazonaws.com"},
				"X-Amz-Date":    {"20150830T123600Z"},
				"Authorization": {"AWS4-HMAC-SHA256 Credential=AKIDEXAMPLE/20150830/us-east-1/service/aws4_request, SignedHeaders=host;x-amz-date, Signature=9c3e54bfcdf0b19771a7f523ee5669cdf59bc7cc0884027167c21bb143a40197"},
			},
			Body: io.NopCloser(bytes.NewReader(nil)),
		},
		// get-vanilla-utf8-query
		{
			URL:    &url.URL{Path: "/", RawQuery: "ሴ=bar"},
			Method: http.MethodGet,
			Header: map[string][]string{
				"Host":          {"example.amazonaws.com"},
				"X-Amz-Date":    {"20150830T123600Z"},
				"Authorization": {"AWS4-HMAC-SHA256 Credential=AKIDEXAMPLE/20150830/us-east-1/service/aws4_request, SignedHeaders=host;x-amz-date, Signature=2cdec8eed098649ff3a119c94853b13c643bcf08f8b0a1d91e12c9027818dd04"},
			},
			Body: io.NopCloser(bytes.NewReader(nil)),
		},
		// normalize path - get relative
		{
			URL:    &url.URL{Path: "/example/.."},
			Method: http.MethodGet,
			Header: map[string][]string{
				"Host":          {"example.amazonaws.com"},
				"X-Amz-Date":    {"20150830T123600Z"},
				"Authorization": {"AWS4-HMAC-SHA256 Credential=AKIDEXAMPLE/20150830/us-east-1/service/aws4_request, SignedHeaders=host;x-amz-date, Signature=5fa00fa31553b73ebf1942676e86291e8372ff2a2260956d9b8aae1d763fbf31"},
			},
			Body: io.NopCloser(bytes.NewReader(nil)),
		},
		// normalize path - get-relative-relative
		{
			URL:    &url.URL{Path: "/example1/example2/../.."},
			Method: http.MethodGet,
			Header: map[string][]string{
				"Host":          {"example.amazonaws.com"},
				"X-Amz-Date":    {"20150830T123600Z"},
				"Authorization": {"AWS4-HMAC-SHA256 Credential=AKIDEXAMPLE/20150830/us-east-1/service/aws4_request, SignedHeaders=host;x-amz-date, Signature=5fa00fa31553b73ebf1942676e86291e8372ff2a2260956d9b8aae1d763fbf31"},
			},
			Body: io.NopCloser(bytes.NewReader(nil)),
		},
		// normalize path - get-slash
		{
			URL:    &url.URL{Path: "//"},
			Method: http.MethodGet,
			Header: map[string][]string{
				"Host":          {"example.amazonaws.com"},
				"X-Amz-Date":    {"20150830T123600Z"},
				"Authorization": {"AWS4-HMAC-SHA256 Credential=AKIDEXAMPLE/20150830/us-east-1/service/aws4_request, SignedHeaders=host;x-amz-date, Signature=5fa00fa31553b73ebf1942676e86291e8372ff2a2260956d9b8aae1d763fbf31"},
			},
			Body: io.NopCloser(bytes.NewReader(nil)),
		},
		// normalize-path - get-slash-dot-slash
		{
			URL:    &url.URL{Path: "/./"},
			Method: http.MethodGet,
			Header: map[string][]string{
				"Host":          {"example.amazonaws.com"},
				"X-Amz-Date":    {"20150830T123600Z"},
				"Authorization": {"AWS4-HMAC-SHA256 Credential=AKIDEXAMPLE/20150830/us-east-1/service/aws4_request, SignedHeaders=host;x-amz-date, Signature=5fa00fa31553b73ebf1942676e86291e8372ff2a2260956d9b8aae1d763fbf31"},
			},
			Body: io.NopCloser(bytes.NewReader(nil)),
		},
		// normalize-path - get-slash-pointless-dot
		{
			URL:    &url.URL{Path: "/./example"},
			Method: http.MethodGet,
			Header: map[string][]string{
				"Host":          {"example.amazonaws.com"},
				"X-Amz-Date":    {"20150830T123600Z"},
				"Authorization": {"AWS4-HMAC-SHA256 Credential=AKIDEXAMPLE/20150830/us-east-1/service/aws4_request, SignedHeaders=host;x-amz-date, Signature=ef75d96142cf21edca26f06005da7988e4f8dc83a165a80865db7089db637ec5"},
			},
			Body: io.NopCloser(bytes.NewReader(nil)),
		},
		// normalize-path - get-slashes
		{
			URL:    &url.URL{Path: "//example//"},
			Method: http.MethodGet,
			Header: map[string][]string{
				"Host":          {"example.amazonaws.com"},
				"X-Amz-Date":    {"20150830T123600Z"},
				"Authorization": {"AWS4-HMAC-SHA256 Credential=AKIDEXAMPLE/20150830/us-east-1/service/aws4_request, SignedHeaders=host;x-amz-date, Signature=9a624bd73a37c9a373b5312afbebe7a714a789de108f0bdfe846570885f57e84"},
			},
			Body: io.NopCloser(bytes.NewReader(nil)),
		},
		// normalize-path - get-space
		{
			URL:    &url.URL{Path: "/example space/"},
			Method: http.MethodGet,
			Header: map[string][]string{
				"Host":          {"example.amazonaws.com"},
				"X-Amz-Date":    {"20150830T123600Z"},
				"Authorization": {"AWS4-HMAC-SHA256 Credential=AKIDEXAMPLE/20150830/us-east-1/service/aws4_request, SignedHeaders=host;x-amz-date, Signature=652487583200325589f1fba4c7e578f72c47cb61beeca81406b39ddec1366741"},
			},
			Body: io.NopCloser(bytes.NewReader(nil)),
		},
		// post-header-key-case
		{
			URL:    &url.URL{Path: "/"},
			Method: http.MethodPost,
			Header: map[string][]string{
				"Host":          {"example.amazonaws.com"},
				"X-Amz-Date":    {"20150830T123600Z"},
				"Authorization": {"AWS4-HMAC-SHA256 Credential=AKIDEXAMPLE/20150830/us-east-1/service/aws4_request, SignedHeaders=host;x-amz-date, Signature=5da7c1a2acd57cee7505fc6676e4e544621c30862966e37dddb68e92efbe5d6b"},
			},
			Body: io.NopCloser(bytes.NewReader(nil)),
		},
		// post-header-key-sort
		{
			URL:    &url.URL{Path: "/"},
			Method: http.MethodPost,
			Header: map[string][]string{
				"Host":          {"example.amazonaws.com"},
				"My-Header1":    {"value1"},
				"X-Amz-Date":    {"20150830T123600Z"},
				"Authorization": {"AWS4-HMAC-SHA256 Credential=AKIDEXAMPLE/20150830/us-east-1/service/aws4_request, SignedHeaders=host;my-header1;x-amz-date, Signature=c5410059b04c1ee005303aed430f6e6645f61f4dc9e1461ec8f8916fdf18852c"},
			},
			Body: io.NopCloser(bytes.NewReader(nil)),
		},
		// post-header-value-case
		{
			URL:    &url.URL{Path: "/"},
			Method: http.MethodPost,
			Header: map[string][]string{
				"Host":          {"example.amazonaws.com"},
				"My-Header1":    {"VALUE1"},
				"X-Amz-Date":    {"20150830T123600Z"},
				"Authorization": {"AWS4-HMAC-SHA256 Credential=AKIDEXAMPLE/20150830/us-east-1/service/aws4_request, SignedHeaders=host;my-header1;x-amz-date, Signature=cdbc9802e29d2942e5e10b5bccfdd67c5f22c7c4e8ae67b53629efa58b974b7d"},
			},
			Body: io.NopCloser(bytes.NewReader(nil)),
		},
		// post-header-sts-token / post-sts-header-after
		{
			URL:    &url.URL{Path: "/"},
			Method: http.MethodPost,
			Header: map[string][]string{
				"Host":                 {"example.amazonaws.com"},
				"X-Amz-Date":           {"20150830T123600Z"},
				"X-Amz-Security-Token": {"AQoDYXdzEPT//////////wEXAMPLEtc764bNrC9SAPBSM22wDOk4x4HIZ8j4FZTwdQWLWsKWHGBuFqwAeMicRXmxfpSPfIeoIYRqTflfKD8YUuwthAx7mSEI/qkPpKPi/kMcGdQrmGdeehM4IC1NtBmUpp2wUE8phUZampKsburEDy0KPkyQDYwT7WZ0wq5VSXDvp75YU9HFvlRd8Tx6q6fE8YQcHNVXAkiY9q6d+xo0rKwT38xVqr7ZD0u0iPPkUL64lIZbqBAz+scqKmlzm8FDrypNC9Yjc8fPOLn9FX9KSYvKTr4rvx3iSIlTJabIQwj2ICCR/oLxBA=="},
				"Authorization":        {"AWS4-HMAC-SHA256 Credential=AKIDEXAMPLE/20150830/us-east-1/service/aws4_request, SignedHeaders=host;x-amz-date, Signature=5da7c1a2acd57cee7505fc6676e4e544621c30862966e37dddb68e92efbe5d6b"},
			},
			Body: io.NopCloser(bytes.NewReader(nil)),
		},
		// post-header-sts-token / post-sts-header-before
		{
			URL:    &url.URL{Path: "/"},
			Method: http.MethodPost,
			Header: map[string][]string{
				"Host":                 {"example.amazonaws.com"},
				"X-Amz-Date":           {"20150830T123600Z"},
				"X-Amz-Security-Token": {"AQoDYXdzEPT//////////wEXAMPLEtc764bNrC9SAPBSM22wDOk4x4HIZ8j4FZTwdQWLWsKWHGBuFqwAeMicRXmxfpSPfIeoIYRqTflfKD8YUuwthAx7mSEI/qkPpKPi/kMcGdQrmGdeehM4IC1NtBmUpp2wUE8phUZampKsburEDy0KPkyQDYwT7WZ0wq5VSXDvp75YU9HFvlRd8Tx6q6fE8YQcHNVXAkiY9q6d+xo0rKwT38xVqr7ZD0u0iPPkUL64lIZbqBAz+scqKmlzm8FDrypNC9Yjc8fPOLn9FX9KSYvKTr4rvx3iSIlTJabIQwj2ICCR/oLxBA=="},
				"Authorization":        {"AWS4-HMAC-SHA256 Credential=AKIDEXAMPLE/20150830/us-east-1/service/aws4_request, SignedHeaders=host;x-amz-date;x-amz-security-token, Signature=85d96828115b5dc0cfc3bd16ad9e210dd772bbebba041836c64533a82be05ead"},
			},
			Body: io.NopCloser(bytes.NewReader(nil)),
		},
		// post-vanilla
		{
			URL:    &url.URL{Path: "/"},
			Method: http.MethodPost,
			Header: map[string][]string{
				"Host":          {"example.amazonaws.com"},
				"X-Amz-Date":    {"20150830T123600Z"},
				"Authorization": {"AWS4-HMAC-SHA256 Credential=AKIDEXAMPLE/20150830/us-east-1/service/aws4_request, SignedHeaders=host;x-amz-date, Signature=5da7c1a2acd57cee7505fc6676e4e544621c30862966e37dddb68e92efbe5d6b"},
			},
			Body: io.NopCloser(bytes.NewReader(nil)),
		},
		// post-vanilla-empty-query-value
		{
			URL:    &url.URL{Path: "/", RawQuery: "Param1=value1"},
			Method: http.MethodPost,
			Header: map[string][]string{
				"Host":          {"example.amazonaws.com"},
				"X-Amz-Date":    {"20150830T123600Z"},
				"Authorization": {"AWS4-HMAC-SHA256 Credential=AKIDEXAMPLE/20150830/us-east-1/service/aws4_request, SignedHeaders=host;x-amz-date, Signature=28038455d6de14eafc1f9222cf5aa6f1a96197d7deb8263271d420d138af7f11"},
			},
			Body: io.NopCloser(bytes.NewReader(nil)),
		},
		// post-vanilla-query
		{
			URL:    &url.URL{Path: "/", RawQuery: "Param1=value1"},
			Method: http.MethodPost,
			Header: map[string][]string{
				"Host":          {"example.amazonaws.com"},
				"X-Amz-Date":    {"20150830T123600Z"},
				"Authorization": {"AWS4-HMAC-SHA256 Credential=AKIDEXAMPLE/20150830/us-east-1/service/aws4_request, SignedHeaders=host;x-amz-date, Signature=28038455d6de14eafc1f9222cf5aa6f1a96197d7deb8263271d420d138af7f11"},
			},
			Body: io.NopCloser(bytes.NewReader(nil)),
		},
		// post-x-www-form-urlencoded
		{
			URL:    &url.URL{Path: "/"},
			Method: http.MethodPost,
			Header: map[string][]string{
				"Content-Type":  {"application/x-www-form-urlencoded"},
				"Host":          {"example.amazonaws.com"},
				"X-Amz-Date":    {"20150830T123600Z"},
				"Authorization": {"AWS4-HMAC-SHA256 Credential=AKIDEXAMPLE/20150830/us-east-1/service/aws4_request, SignedHeaders=content-type;host;x-amz-date, Signature=ff11897932ad3f4e8b18135d722051e5ac45fc38421b1da7b9d196a0fe09473a"},
			},
			Body: io.NopCloser(strings.NewReader("Param1=value1")),
		},
	} {
		tc := tc
		t.Run(fmt.Sprintf("case-%d", idx), func(t *testing.T) {
			t.Parallel()
			accessKeyID, err := validator.Validate(context.Background(), &tc)
			if err != nil {
				t.Error(err)
				return
			}
			if accessKeyID != "AKIDEXAMPLE" {
				t.Errorf("unexpected access Key ID: %q, expect: %q", accessKeyID, "AKIDEXAMPLE")
			}
		})
	}
}
