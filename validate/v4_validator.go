package validate

import (
	"bytes"
	"context"
	"crypto/sha256"
	"encoding/hex"
	"errors"
	"io"
	"net/http"
	"path"
	"sort"
	"strings"
	"time"

	"gitlab.com/oss-cloud/aws/signature/common"
)

// Signature and API related constants.
const (
	signV4Algorithm   = "AWS4-HMAC-SHA256"
	iso8601DateFormat = "20060102T150405Z"
	yyyymmdd          = "20060102"
)

// unsignedPayload - value to be set to X-Amz-Content-Sha256 header when
const unsignedPayload = "UNSIGNED-PAYLOAD"

var (
	ErrInvalidDate              = errors.New("v4: invalid date")
	ErrInvalidHeader            = errors.New("v4: invalid header")
	ErrInvalidService           = errors.New("v4: invalid service")
	ErrInvalidLocation          = errors.New("v4: invalid location")
	ErrSignatureMismatch        = errors.New("v4: signature mismatch")
	ErrAuthHeaderEmpty          = errors.New("v4: auth header empty")
	ErrSignatureV4InvalidFormat = errors.New("v4: signature invalid format")
)

// getSigningKey hmac seed to calculate final signature.
func getSigningKey(secret, loc string, t time.Time, serviceType string) []byte {
	date := sumHMAC([]byte("AWS4"+secret), []byte(t.Format(yyyymmdd)))
	location := sumHMAC(date, []byte(loc))
	service := sumHMAC(location, []byte(serviceType))
	signingKey := sumHMAC(service, []byte("aws4_request"))
	return signingKey
}

// getSignature final signature in hexadecimal form.
func getSignature(signingKey []byte, stringToSign string) string {
	return hex.EncodeToString(sumHMAC(signingKey, []byte(stringToSign)))
}

// getScope generate a string of a specific date, an AWS region, and a
// service.
func getScope(location string, t time.Time, serviceType string) string {
	scope := strings.Join([]string{
		t.Format(yyyymmdd),
		location,
		serviceType,
		"aws4_request",
	}, "/")
	return scope
}

// getHashedPayload get the hexadecimal value of the SHA256 hash of
// the request payload.
func getHashedPayload(req *http.Request) (hashedPayload string) {
	hashedPayload = req.Header.Get(common.AmzContentSha256)
	if hashedPayload != "" {
		return hashedPayload
	}

	var buffer bytes.Buffer
	hasher := sha256.New()
	r := io.TeeReader(req.Body, hasher)
	_, _ = io.Copy(&buffer, r)

	req.Body = io.NopCloser(&buffer)

	return hex.EncodeToString(hasher.Sum(nil))
}

// getCanonicalHeaders generate a list of request headers for
// signature.
func getCanonicalHeaders(req *http.Request, signedHeaders []string) string {
	builder := stringBuilderPool.Get().(*strings.Builder)
	defer stringBuilderPool.Put(builder)
	builder.Reset()

	for _, key := range signedHeaders {
		builder.WriteString(key)
		builder.WriteByte(':')
		values := req.Header[http.CanonicalHeaderKey(key)]
		trimmedValues := make([]string, 0, len(values))
		for _, value := range values {
			trimmedValues = append(trimmedValues, signV4TrimAll(value))
		}
		builder.WriteString(strings.Join(trimmedValues, ","))
		builder.WriteByte('\n')
	}

	return builder.String()
}

// getSignedHeaders generate all signed request headers.
// i.e. lexically sorted, semicolon-separated list of lowercase
// request header names.
func getSignedHeaders(req *http.Request, ignoredHeaders []string) string {
	var headers []string
	for k := range req.Header {
		if isInStringList(http.CanonicalHeaderKey(k), ignoredHeaders) {
			continue // Ignored header found continue.
		}
		headers = append(headers, strings.ToLower(k))
	}
	sort.Strings(headers)
	return strings.Join(headers, ";")
}

func getOrderQuery(req *http.Request) string {
	query := req.URL.Query()
	for _, values := range query {
		sort.Strings(values)
	}
	return strings.ReplaceAll(query.Encode(), "+", "%20")
}

func getNormalizePath(rawPath string) string {
	cleanPath := path.Clean(rawPath)
	if len(cleanPath) > 2 && strings.HasSuffix(rawPath, "/") {
		cleanPath = cleanPath + "/"
	}
	encoded := encodePath(cleanPath)
	return encoded
}

func getCanonicalRequest(req *http.Request, signedHeaders []string, hashedPayload string) string {
	queryString := getOrderQuery(req)

	canonicalRequest := strings.Join([]string{
		req.Method,
		getNormalizePath(req.URL.Path),
		queryString,
		getCanonicalHeaders(req, signedHeaders),
		strings.Join(signedHeaders, ";"),
		hashedPayload,
	}, "\n")
	return canonicalRequest
}

// getStringToSign a string based on selected query values.
func getStringToSignV4(t time.Time, location, canonicalRequest, serviceType string) string {
	builder := stringBuilderPool.Get().(*strings.Builder)
	defer stringBuilderPool.Put(builder)
	builder.Reset()
	builder.WriteString(signV4Algorithm)
	builder.WriteByte('\n')
	builder.WriteString(t.Format(iso8601DateFormat))
	builder.WriteByte('\n')
	builder.WriteString(getScope(location, t, serviceType))
	builder.WriteByte('\n')
	builder.WriteString(sum256(canonicalRequest))
	return builder.String()
}

type V4Validator struct {
	Service  string
	Location string
	Locator  KeyLocator

	Clock     ClockFunc
	AllowDiff time.Duration
}

func (v *V4Validator) Validate(ctx context.Context, req *http.Request) (accessKeyID string, err error) {
	authorization, err := v.extractAuthorization(req)
	if err != nil {
		return
	}

	clockFunc := v.Clock
	if clockFunc == nil {
		clockFunc = time.Now
	}
	now := clockFunc().UTC()

	parsedDate, err := time.Parse(iso8601DateFormat, req.Header.Get(common.AmzDate))
	if err != nil {
		return
	}

	allowDiff := v.AllowDiff
	if allowDiff == 0 {
		allowDiff = time.Minute
	}

	if now.Sub(parsedDate) > allowDiff {
		return "", ErrInvalidDate
	}

	sessionToken := req.Header.Get(common.AmzSecurityToken)
	secretAccessKey, err := v.Locator.FetchSecretKey(ctx, authorization.AccessKeyID, sessionToken)
	if err != nil {
		return
	}

	hashedPayload := getHashedPayload(req)

	if v.Service == "sts" && req.Header.Get(common.AmzContentSha256) != "" {
		return "", ErrInvalidHeader
	}

	// Get canonical request.
	canonicalRequest := getCanonicalRequest(req, authorization.SignedHeaders, hashedPayload)

	// Get string to sign from canonical request.
	stringToSign := getStringToSignV4(parsedDate, v.Location, canonicalRequest, v.Service)

	// Get hmac signing key.
	signingKey := getSigningKey(secretAccessKey, v.Location, parsedDate, v.Service)

	// Calculate signature.
	signature := getSignature(signingKey, stringToSign)

	if signature != authorization.Signature {
		return "", ErrSignatureMismatch
	}

	return authorization.AccessKeyID, nil
}

func (v *V4Validator) extractAuthorization(req *http.Request) (signature v4Authorization, err error) {
	authorization := req.Header.Get(common.Authorization)

	if authorization == "" {
		return v4Authorization{}, ErrAuthHeaderEmpty
	}

	if !strings.HasPrefix(authorization, signV4Algorithm+" ") {
		return v4Authorization{}, ErrInvalidHeader
	}
	auth := strings.TrimPrefix(authorization, signV4Algorithm)
	auth = strings.TrimSpace(auth)
	pieces := strings.Split(auth, ", ")

	if len(pieces) != 3 {
		return v4Authorization{}, ErrSignatureV4InvalidFormat
	}

	signature = v4Authorization{}

	for idx, piece := range pieces {
		switch idx {
		case 0: // Credential
			content := strings.TrimPrefix(piece, "Credential=")
			chunk := strings.Split(content, "/")
			if len(chunk) != 5 {
				return v4Authorization{}, ErrSignatureV4InvalidFormat
			}
			signature.AccessKeyID = chunk[0]
			signature.AmzDate = chunk[1]
			signature.Location = chunk[2]
			signature.Service = chunk[3]
		case 1: // SignedHeaders
			content := strings.TrimPrefix(piece, "SignedHeaders=")
			signature.SignedHeaders = strings.Split(content, ";")
		case 2: // Signature
			content := strings.TrimPrefix(piece, "Signature=")
			signature.Signature = content
		}
	}

	if signature.Service != v.Service {
		return v4Authorization{}, ErrInvalidService
	}

	if signature.Location != v.Location {
		return v4Authorization{}, ErrInvalidLocation
	}

	signature.Auth = authorization

	return
}

type v4Authorization struct {
	Auth,
	AccessKeyID,
	AmzDate,
	Location,
	Service,
	Signature string
	SignedHeaders []string
}
